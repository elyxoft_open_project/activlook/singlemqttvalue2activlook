from argparse import *

class ArgumentParser(ArgumentParser):

    def exit(self, status=0, message=None):
        pass
    
    def error(self, message):
        """error(message: string)

        Raise an exception.
        """
        raise ArgumentError(None, message)