import aiomqtt as mqtt
import logging
import traceback

class MQTT:
    def __init__(self, broker_uri: str, username: str, password: str):
        self.broker_uri: str = broker_uri
        self.username: str = username
        self.password: str = password
        self.client: mqtt.Client = None

    async def connect(self):
        """Connect to the MQTT broker."""
        try:
            async with mqtt.Client(self.broker_uri, username=self.username, password=self.password) as client:
                self.client = client
                logging.info(f"Connected to {self.broker_uri}")
        except mqtt.MqttError as error:
            logging.error(f"Error: {error}")
            self.client = None

    async def subscribe(self, topic: str, func):
        """Subscribe to a topic and handle incoming messages with the provided callback.        
        Args:
            topic (str): The topic to subscribe to.
            func (function): The callback function to handle incoming messages.
        """
        if self.client is None:
            logging.error("Client is not connected.")
            return

        async with self.client as client:
            await client.subscribe(topic)
            logging.info(f"Subscribed to topic: {topic}")
            async for message in client.messages:
                logging.info(f"Topic: {message.topic}\nMessage: {message.payload.decode()}")
                try:
                    await func(message)
                except Exception as e:
                    logging.error(f"ERROR during execution of the handler function associated with the topic {message.topic}\nERROR: {traceback.format_exc()}")
