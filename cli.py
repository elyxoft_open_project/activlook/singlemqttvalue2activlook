from typing import Callable, List
import logging

class CommandManager:
    def __init__(self):
        self.commands = {}

    def register(self, name: str):
        def decorator(func: Callable):
            self.commands[name] = func
            return func
        return decorator

    async def execute(self, name: str, *args):
        if name in self.commands:
            await self.commands[name](*args)
        else:
            logging.error(f"Unknown command: {name}")
