# SingleMqttValue2ActivLook

SingleMqttValue2ActivLook is a Python script designed to visualise MQTT data on ActiveLook smart glasses. 
It provides a command line interface to manage display settings, graphs and configurations.

For more information about ActiveLook glasses and developer resources, visit the [ActiveLook website](https://www.activelook.net/) and check out the [developer documentation](https://github.com/ActiveLook/Activelook-API-Documentation/blob/main/ActiveLook_API.md).

## How to Use?

To use this Python script with your ActiveLook glasses, follow these steps:

### Prerequisites

- Python 3.10 or newer installed

### Install

0. **Clone the Repository:**
   Open your terminal and run the following command to clone the repository to your local machine and navigate to the project directory:
   ```bash
   git clone https://gitlab.com/elyxoft_open_project/activlook/singlemqttvalue2activlook.git
   cd singlemqttvalue2activlook
   ```

1. **Set Up Virtual Environment:**
   Create a virtual environment (venv) to manage dependencies locally.
   ```bash
   python -m venv venv
   ```

2. **Install Requirements:**
   Activate the virtual environment and install the required packages from `requirements.txt`.
   ```bash
   source venv/bin/activate  # On Windows use `venv\Scripts\activate.bat`
   pip install -r requirements.txt
   ```

3. **Run the Script:**
   Start the `main.py` script with the necessary arguments to connect to the MQTT broker and subscribe to a specific topic.
   ```bash
   python main.py broker_uri username password topic
   ```
   - `broker_uri`: URI of the MQTT broker (e.g., `broker.example.com`)
   - `username`: Username for authentication (if required by the broker)
   - `password`: Password for authentication (if required by the broker)
   - `topic`: MQTT topic to subscribe to

   This script retrieves numeric values published on the specified MQTT topic and displays them on your ActiveLook glasses.

4. **Exiting the Script:**
   To stop the script, use `Ctrl + C` in the terminal where it's running.

## CLI

This tool also provides a command-line interface for interacting with ActiveLook glasses. Not all commands are currently available. Here is the list of implemented commands.

### General Commands

`display_power`

    Description: Enable/disable power of the display.
    Argument:
        enable: Enable (1) or disable (0) display power.

`clear`

    Description: Clear the display memory (black screen).

`grey`

    Description: Set display grey level.
    Argument:
        level: Grey level (0 to 15).

`battery`

    Description: Get the battery level in %.

`version`

    Description: Get the device ID and firmware version.

`led`

    Description: Set LED state.
    Argument:
        state: LED state (0: Off, 1: On, 2: Toggle, 3: Blinking).

`shift`

    Description: Shift display objects.
    Arguments:
        -x: (default: 0, range: -128 to 127) - Shift in x direction.
        -y: (default: 0, range: -128 to 127) - Shift in y direction.

`settings`

    Description: Return the user parameters (shift, luma, sensor).

### Display Luminance Commands

`luma`

    Description: Set display luminance.
    Argument:
        level: Luminance level (0 to 15).

Optical Sensor Commands
`sensor`

    Description: Turn on/off auto-brightness adjustment and gesture detection.
    Argument:
        enable: Enable (1) or disable (0) auto-brightness adjustment and gesture detection.

`gesture`

    Description: Turn on/off gesture detection.
    Argument:
        enable: Enable (1) or disable (0) gesture detection.

`auto_brightness`

    Description: Turn on/off auto-brightness adjustment.
    Argument:
        enable: Enable (1) or disable (0) auto-brightness adjustment.

### Graphics Commands

`color`

    Description: Sets the grey level used to draw the next graphical element.
    Argument:
        color: Grey level (0 to 15).

`point`

    Description: Set a pixel at the corresponding coordinates.
    Arguments:
        x: X coordinate.
        y: Y coordinate.

`line`

    Description: Draw a line at the corresponding coordinates.
    Arguments:
        x0: X0 coordinate.
        y0: Y0 coordinate.
        x1: X1 coordinate.
        y1: Y1 coordinate.

`rect`

    Description: Draw an empty rectangle at the corresponding coordinates.
    Arguments:
        x0: X0 coordinate.
        y0: Y0 coordinate.
        x1: X1 coordinate.
        y1: Y1 coordinate.

`rectf`

    Description: Draw a full rectangle at the corresponding coordinates.
    Arguments:
        x0: X0 coordinate.
        y0: Y0 coordinate.
        x1: X1 coordinate.
        y1: Y1 coordinate.

`circ`

    Description: Draw an empty circle at the corresponding coordinates.
    Arguments:
        x: X coordinate.
        y: Y coordinate.
        r: Radius.

`circf`

    Description: Draw a full circle at the corresponding coordinates.
    Arguments:
        x: X coordinate.
        y: Y coordinate.
        r: Radius.

`text`

    Description: Write text to display.
    Arguments:
        -x: X coordinate (default: 274).
        -y: Y coordinate (default: 231).
        -d: Direction (default: 4).
        -f: Font (default: 2).
        -c: Color (default: 15).
        text: Text to display.

`polyline`

    Description: Draw multiple connected lines at the corresponding coordinates.
    Arguments:
        -t: Line thickness (default: 1).
        coordinates: Coordinates for drawing lines. Provide pairs of x and y coordinates.

`hold_flush`

    Description: Hold or flush the graphic engine.
    Argument:
        action:
            0: Hold display.
            1: Flush display.
            0xFF: Reset and flush all stacked hold.

`arc`

    Description: Draw an arc circle at the corresponding coordinates.
    Arguments:
        x: X coordinate.
        y: Y coordinate.
        r: Radius.
        angle_start: Angle start (in degrees).
        angle_end: Angle end (in degrees).
        thickness: Thickness.

### Layout Commands

`layout_delete`

    Description: Delete a layout.
    Argument:
        id: ID of the layout to be deleted. If id = 0xFF, delete all layouts.

`layout_display`

    Description: Display text with layout id parameters.
    Arguments:
        id: ID of the layout.
        text: Text to display.

`layout_clear`

    Description: Clear the screen of the corresponding layout area.
    Argument:
        id: ID of the layout.

`layout_list`

    Description: Give the list of saved layouts.

`layout_position`

    Description: Redefine the position of a layout.
    Arguments:
        id: ID of the layout.
        x: X coordinate.
        y: Y coordinate.

`layout_get`

    Description: Get a layout parameters.
    Argument:
        id: ID of the layout.

`layout_clear_extended`

    Description: Clear the screen of the corresponding layout area.
    Arguments:
        id: ID of the layout.
        x: X coordinate.
        y: Y coordinate.

`layout_clear_display`

    Description: Clear area and display text with layout id parameters.
    Arguments:
        id: ID of the layout.
        text: Text to display.

### Configuration Commands

`cfg_write`

    Description: Write configuration, configurations are associated with layouts, images, etc.
    Arguments:
        name: Name of the configuration (max length: 12).
        version: Version to track versions of configuration.
        password: If configuration already exists, the same password must be provided as the one used during the creation.

`cfg_read`

    Description: Get the number of elements stored in the configuration.
    Argument:
        name: Name of the configuration (max length: 12).

`cfg_set`

    Description: Select the current configuration used to display layouts, images, etc.
    Argument:
        name: Name of the configuration (max length: 12).

`cfg_list`

    Description: List configurations in memory.

`cfg_rename`

    Description: Rename a configuration.
    Arguments:
        old_name: Old name of the configuration (max length: 12).
        new_name: New name of the configuration (max length: 12).

`cfg_delete`

    Description: Delete a configuration and all elements associated.
    Argument:
        name: Name of the configuration (max length: 12).

`cfg_delete_less_used`

    Description: Delete the configuration that has not been used for the longest time.

`cfg_free_space`

    Description: Get free space available to store layouts, images, etc.

`cfg_get_nb`

    Description: Get number of configurations in memory.

### Device Commands

`shutdown`

    Description: Shutdown the device.

`reset`

    Description: Reset the device.

`read_device_info`

    Description: Read a device information parameter.
    Argument:
        id: Identifier of the parameter to read.

### Other
 
`send_raw_frame`

    Description: Send raw frame.
    Argument:
        frame: Hexadecimal raw frame.

## License
Distributed under the MIT License. See LICENSE for more information.