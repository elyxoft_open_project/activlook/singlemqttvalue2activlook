

class CommandEvent:
    def __init__(self, command_id, data):
        self.command_id = command_id
        self.data = data

class EventDispatcher:
    def __init__(self):
        self._listeners = []

    def register(self, listener):
        self._listeners.append(listener)

    async def dispatch(self, event):
        for listener in self._listeners:
            await listener(event)