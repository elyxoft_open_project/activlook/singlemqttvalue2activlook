import asyncio
from bleak import BleakScanner, BleakClient, BleakGATTCharacteristic, BleakError
import logging

from event import CommandEvent, EventDispatcher

class GlassesCommandException(Exception):
    def __init__(self, message: str) -> None:
        self.message = message

    def __str__(self) -> str:
        return "GlassesCommandException: " + self.message
    

# ff 60 00 1f 0a 1a 012c 1e 0128 c8 0f 00 00 00 0000 00 00 00 07 0000 0000 0128 00c8 aa
class GlassesLayoutParameters:
    def __init__(self, id: int, x: int, y: int, width: int, height: int, fore_color: int, back_color: int, font: int, text_valid: bool, text_x: int, text_y: int, text_rotation: int, text_opacity: bool):
        self.id: int = id
        self.x: int = x
        self.y: int = y
        self.width: int = width
        self.height: int = height
        self.fore_color: int = fore_color
        self.back_color: int = back_color
        self.font: int = font
        self.text_valid: bool = text_valid
        self.text_x: int = text_x
        self.text_y: int = text_y
        self.text_rotation: int = text_rotation
        self.text_opacity: bool = text_opacity
        self.graph_commands = []

    def build_parameters(self):
        command: bytearray = bytearray()
        command.extend(self.id.to_bytes(1, byteorder='big'))
        command.extend(int(0).to_bytes(1, byteorder='big')) # temp byte for size
        command.extend(self.x.to_bytes(2, byteorder='big'))
        command.extend(self.y.to_bytes(1, byteorder='big'))
        command.extend(self.width.to_bytes(2, byteorder='big'))
        command.extend(self.height.to_bytes(1, byteorder='big'))
        command.extend(self.fore_color.to_bytes(1, byteorder='big'))
        command.extend(self.back_color.to_bytes(1, byteorder='big'))
        command.extend(self.font.to_bytes(1, byteorder='big'))
        command.extend(self.text_valid.to_bytes(1, byteorder='big'))
        command.extend(self.text_x.to_bytes(2, byteorder='big'))
        command.extend(self.text_y.to_bytes(1, byteorder='big'))
        command.extend(self.text_rotation.to_bytes(1, byteorder='big'))
        command.extend(self.text_opacity.to_bytes(1, byteorder='big'))

        size = 0
        for cmd in self.graph_commands:
            match cmd["cmd_id"]:
                case 0: # image
                    command.extend(cmd["cmd_id"].to_bytes(1) + cmd["img_id"].to_bytes(1) + cmd["x"].to_bytes(2, signed=True) + cmd["y"].to_bytes(2, signed=True))
                    size += 6
                case 1: # circ
                    command.extend(cmd["cmd_id"].to_bytes(1) + cmd["x"].to_bytes(2, signed=True) + cmd["y"].to_bytes(2, signed=True) + cmd["r"].to_bytes(2))
                    size += 7
                case 2: # circf
                    command.extend(cmd["cmd_id"].to_bytes(1) + cmd["x"].to_bytes(2, signed=True) + cmd["y"].to_bytes(2, signed=True) + cmd["r"].to_bytes(2))
                    size += 7
                case 3: # color
                    command.extend(cmd["cmd_id"].to_bytes(1) + cmd["color"].to_bytes(1))
                    size += 2
                case 4: # font
                    command.extend(cmd["cmd_id"].to_bytes(1) + cmd["font"].to_bytes(1))
                    size +=2
                case 5: # line
                    command.extend(cmd["cmd_id"].to_bytes(1) + cmd["x0"].to_bytes(2, signed=True) + cmd["y0"].to_bytes(2, signed=True) + cmd["x1"].to_bytes(2, signed=True) + cmd["y1"].to_bytes(2, signed=True))
                    size += 9
                case 6: # point
                    command.extend(cmd["cmd_id"].to_bytes(1) + cmd["x"].to_bytes(2, signed=True) + cmd["y"].to_bytes(2, signed=True))
                    size += 5
                case 7: # rect
                    command.extend(cmd["cmd_id"].to_bytes(1) + cmd["x0"].to_bytes(2, signed=True) + cmd["y0"].to_bytes(2, signed=True) + cmd["x1"].to_bytes(2, signed=True) + cmd["y1"].to_bytes(2, signed=True))
                    size += 9
                case 8: # rectf
                    command.extend(cmd["cmd_id"].to_bytes(1) + cmd["x0"].to_bytes(2, signed=True) + cmd["y0"].to_bytes(2, signed=True) + cmd["x1"].to_bytes(2, signed=True) + cmd["y1"].to_bytes(2, signed=True))
                    size += 9
                case 9: # text
                    command.extend(cmd["cmd_id"].to_bytes(1) + cmd["x"].to_bytes(2, signed=True) + cmd["y"].to_bytes(2, signed=True) + len(cmd["text"]).to_bytes(1) + cmd["text"].encode('ascii'))
                    size += 6 + len(cmd["text"])
                case 10: # gauge
                    command.extend(cmd["cmd_id"].to_bytes(1) + cmd["gauge_id"].to_bytes(1))
                    size += 2
                case 11: # anim
                    command.extend(cmd["cmd_id"].to_bytes(1) + cmd["handler_id"].to_bytes(1) + cmd["id"].to_bytes(1) + cmd["delay"].to_bytes(2) + cmd["repeat"].to_bytes(1) + cmd["x"].to_bytes(2, signed=True) + cmd["y"].to_bytes(2, signed=True))
                    size += 10
                case 12: # polyline
                    reserved = 0
                    command.extend(cmd["cmd_id"].to_bytes(1) + len(cmd["points"]).to_bytes(1) + cmd["thickness"].to_bytes(1) + reserved.to_bytes(1) + reserved.to_bytes(1))
                    size += 5
                    for point in cmd["points"]:
                        command.extend(point[0].to_bytes(2, signed=True) + point[1].to_bytes(2, signed=True))
                        size += 4

        command[1] = size
        logging.debug(f"Layout command size: {size}")
        return command

    def add_image(self, img_id, x, y):
        self.graph_commands.append({
            "cmd_id": 0,
            "img_id": img_id,
            "x": x,
            "y": y
        })

    def add_circ(self, x, y, r):
        self.graph_commands.append({
            "cmd_id": 1,
            "x": x,
            "y": y,
            "r": r
        })

    def add_circf(self, x, y, r):
        self.graph_commands.append({
            "cmd_id": 2,
            "x": x,
            "y": y,
            "r": r
        })

    def add_color(self, color):
        self.graph_commands.append({
            "cmd_id": 3,
            "color": color
        })

    def add_font(self, font):
        self.graph_commands.append({
            "cmd_id": 4,
            "font": font
        })

    def add_line(self, x0, y0, x1, y1):
        self.graph_commands.append({
            "cmd_id": 5,
            "x0": x0,
            "y0": y0,
            "x1": x1,
            "y1": y1,
        })

    def add_point(self, x, y):
        self.graph_commands.append({
            "cmd_id": 6,
            "x": x,
            "y": y
        })

    def add_rect(self, x0, y0, x1, y1):
        self.graph_commands.append({
            "cmd_id": 7,
            "x0": x0,
            "y0": y0,
            "x1": x1,
            "y1": y1,
        })

    def add_rectf(self, x0, y0, x1, y1):
        self.graph_commands.append({
            "cmd_id": 8,
            "x0": x0,
            "y0": y0,
            "x1": x1,
            "y1": y1,
        })

    def add_text(self, x, y, text):
        self.graph_commands.append({
            "cmd_id": 9,
            "x": x,
            "y": y,
            "text": text
        })

    def add_gauge(self, gauge_id):
        self.graph_commands.append({
            "cmd_id": 10,
            "gauge_id": gauge_id
        })

    def add_anim(self, handler_id, id, delay, repeat, x, y):
        self.graph_commands.append({
            "cmd_id": 11,
            "handler_id": handler_id,
            "id": id,
            "delay": delay,
            "repeat": repeat,
            "x": x,
            "y": y        
        })

    async def add_polyline(self, thickness: int, points: list[tuple[int, int]]):
        self.graph_commands.append({
            "cmd_id": 12,
            "thickness": thickness,
            "points": points
        })

    
class Glasses:
    def __init__(self):
        self.device_address: str = ""
        self.rx_char: str = "0783B03E-8535-B5A0-7140-A304D2495CBA"
        self.tx_char: str = "0783B03E-8535-B5A0-7140-A304D2495CB8"
        self.client: BleakClient = None
        self.dispatcher: EventDispatcher = None

        self.current_received_frame: bytearray = b''

    @classmethod
    async def create(cls, dispatcher: EventDispatcher):
        self = cls()
        self.dispatcher = dispatcher
        logging.info("Scanning for devices...")
        glasses = await self.scan()

        if not glasses:
            logging.error("No Active Look device found!")
            return

        if len(glasses) == 1:
            logging.info(f"Active Look device found: {glasses[0]}")
            self.device_address = glasses[0].address
        else:
            logging.info("\nChoose your Active Look device:")
            for i, device in enumerate(glasses):
                logging.info(f"[{i}] {device}")
            user_choice = self._get_user_choice(len(glasses) - 1)
            self.device_address = glasses[user_choice].address

        self.client = BleakClient(self.device_address, disconnected_callback=self.disconnect_handler)
        await self.connect()

        await self.clear()
        return self        

    def _get_user_choice(self, max_choice: int) -> int:
        while True:
            try:
                choice = int(input(f"Which device do you want to connect to? (0 - {max_choice}): "))
                if 0 <= choice <= max_choice:
                    return choice
            except ValueError:
                pass
            logging.error(f"Please choose a valid number between 0 and {max_choice}.")

    async def scan(self):
        devices = await BleakScanner.discover()
        return [device for device in devices if device.name and device.name.startswith("A.LooK")]

    async def connect(self):
        try:
            await self.client.connect()
            if self.client.is_connected:
                logging.info(f"Connected to {self.client.address}")
                self.client.mtu_size
                await self.client.start_notify(self.tx_char, self.tx_char_handler)
                asyncio.create_task(self.keep_alive())
            else:
                logging.error(f"Failed to connect to {self.device_address}")
        except Exception as e:
            logging.error(f"Exception during connect: {e}")

    def disconnect_handler(self, client: BleakClient):
        logging.warning(f"Disconnected from {client.address}")

    async def keep_alive(self):
        while True:
            await asyncio.sleep(10)
            if not self.client.is_connected:
                logging.info("Reconnecting...")
                try:
                    await self.client.connect()
                    if self.client.is_connected:
                        logging.info(f"Reconnected to {self.client.address}")
                        await self.client.stop_notify(self.tx_char)
                        await self.client.start_notify(self.tx_char, self.tx_char_handler)
                    else:
                        logging.error(f"Failed to reconnect to {self.device_address}")
                except Exception as e:
                    logging.error(f"Exception during reconnect: {e}")

# #################################################################################################
# Communication Master -> ActiveLook

    async def send_data(self, data: bytearray):
        if not self.device_address or not self.client:
            logging.error("ERROR: Cannot send data. Device address or client is empty.")
            return

        if self.client.is_connected:
            try:
                await self.client.write_gatt_char(self.rx_char, data)
                logging.debug(f"Data sent to {self.rx_char}\nDATA: {data.hex()}")
            except BleakError as e:
                print(f"BLE error occurred: {e}")

    def build_command(self, command_id: int, command_format: int, data: bytes = b'', query_id: bytes = b'') -> bytearray:
        start = b'\xFF'
        footer = b'\xAA'

        length = 5 + len(data) + len(query_id)
        if length > 0xFF:
            command_format |= (1 << 4)
        length_bytes = length.to_bytes(1 if (command_format & (1 << 4)) == 0 else 2, byteorder='big')
        command = start + command_id.to_bytes(1, byteorder='big') + command_format.to_bytes(1, byteorder='big') + length_bytes + query_id + data + footer
        logging.debug(f"Command builded: {command.hex()}")
        return command

# ================
# Command function
# -------
# General

    async def display_power(self, enable: int):
        """Enable / disable power of the display"""
        command = self.build_command(0x00, 0x00, enable.to_bytes(1, byteorder='big'))
        await self.send_data(command)

    async def clear(self):
        """Clear the display memory (black screen)"""
        command = self.build_command(0x01, 0x00)
        await self.send_data(command)

    async def grey(self, level: int):
        """Set the whole display to the corresponding grey level (0 to 15)"""
        command = self.build_command(0x02, 0x00, level.to_bytes(1, byteorder='big'))
        await self.send_data(command)

    async def battery(self):
        """Get the battery level in %"""
        command = self.build_command(0x05, 0x00)
        await self.send_data(command)

    async def version(self):
        """Get the device ID and firmware version"""
        command = self.build_command(0x06, 0x00)
        await self.send_data(command)

    async def led(self, state: int):
        """Set green LED:
            0: Off
            1: On
            2: Toggle
            3: Blinking
        """
        command = self.build_command(0x08, 0x00, state.to_bytes(1, byteorder='big'))
        await self.send_data(command)

    async def shift(self, x: int, y: int):
        """Shift all subsequently displayed objects of (x,y) pixels.
           Valid values are between -128 and 127
        """
        data = x.to_bytes(2, byteorder='big', signed=True) + y.to_bytes(2, byteorder='big', signed=True)
        command = self.build_command(0x09, 0x00, data)
        await self.send_data(command)

    async def settings(self):
        """Return the user parameters (shift, luma, sensor)"""
        command = self.build_command(0x0A, 0x00)
        await self.send_data(command)

# -----------------
# Display luminance

    async def luma(self, level: int):
        """Set the display luminance to the corresponding level (0 to 15)"""
        command = self.build_command(0x10, 0x00, level.to_bytes(1, byteorder='big'))
        await self.send_data(command)

# --------------
# Optical sensor

    async def sensor(self, enable: bool):
        """Turn on/off the auto-brightness adjustment and gesture detection"""
        command = self.build_command(0x20, 0x00, enable.to_bytes(1, byteorder='big'))
        await self.send_data(command)

    async def gesture(self, enable: bool):
        """Turn on/off only the gesture detection"""
        command = self.build_command(0x21, 0x00, enable.to_bytes(1, byteorder='big'))
        await self.send_data(command)

    async def auto_brightness(self, enable: bool):
        """Turn on/off only the auto-brightness adjustment"""
        command = self.build_command(0x22, 0x00, enable.to_bytes(1, byteorder='big'))
        await self.send_data(command)

# --------
# Graphics

    async def color(self, color: int):
        """Sets the grey level (0 to 15) used to draw the next graphical element"""
        command = self.build_command(0x30, 0x00, color.to_bytes(1, byteorder='big'))
        await self.send_data(command)

    async def point(self, x: int, y: int):
        """Set a pixel on at the corresponding coordinates"""
        data = x.to_bytes(2, byteorder='big') + y.to_bytes(2, byteorder='big')
        command = self.build_command(0x31, 0x00, data)
        await self.send_data(command)

    async def line(self, x0: int, y0: int, x1: int, y1: int):
        """Draw a line at the corresponding coordinates"""
        data = (x0.to_bytes(2, byteorder='big') + y0.to_bytes(2, byteorder='big') +
                x1.to_bytes(2, byteorder='big') + y1.to_bytes(2, byteorder='big'))
        command = self.build_command(0x32, 0x00, data)
        await self.send_data(command)

    async def rect(self, x0: int, y0: int, x1: int, y1: int):
        """Draw an empty rectangle at the corresponding coordinates"""
        data = (x0.to_bytes(2, byteorder='big') + y0.to_bytes(2, byteorder='big') +
                x1.to_bytes(2, byteorder='big') + y1.to_bytes(2, byteorder='big'))
        command = self.build_command(0x33, 0x00, data)
        await self.send_data(command)

    async def rectf(self, x0: int, y0: int, x1: int, y1: int):
        """Draw a full rectangle at the corresponding coordinates"""
        data = (x0.to_bytes(2, byteorder='big') + y0.to_bytes(2, byteorder='big') +
                x1.to_bytes(2, byteorder='big') + y1.to_bytes(2, byteorder='big'))
        command = self.build_command(0x34, 0x00, data)
        await self.send_data(command)

    async def circ(self, x: int, y: int, r: int):
        """Draw an empty circle at the corresponding coordinates"""
        data = (x.to_bytes(2, byteorder='big') + y.to_bytes(2, byteorder='big') +
                r.to_bytes(1, byteorder='big'))
        command = self.build_command(0x35, 0x00, data)
        await self.send_data(command)

    async def circf(self, x: int, y: int, r: int):
        """Draw a full circle at the corresponding coordinates"""
        data = (x.to_bytes(2, byteorder='big') + y.to_bytes(2, byteorder='big') +
                r.to_bytes(1, byteorder='big'))
        command = self.build_command(0x36, 0x00, data)
        await self.send_data(command)

    async def text(self, x: int, y: int, direction: int, font: int, color: int, text: str):
        """Write text string at coordinates (x,y) with rotation, font size, and color"""
        data = (x.to_bytes(2, byteorder='big') + y.to_bytes(2, byteorder='big') +
                direction.to_bytes(1, byteorder='big') + font.to_bytes(1, byteorder='big') +
                color.to_bytes(1, byteorder='big') + text.encode('ascii'))
        command = self.build_command(0x37, 0x00, data)
        await self.send_data(command)

    async def polyline(self, thickness: int, coordinates: list):
        """Draw multiple connected lines at the corresponding coordinates. Thickness is set for all lines."""
        data = thickness.to_bytes(1, byteorder='big') + 0x00.to_bytes(2, byteorder='big') # Two bytes reserved for backward compatibility

        for arg in coordinates:
            data += arg.to_bytes(2, byteorder='big')

        command = self.build_command(0x38, 0x00, data)
        await self.send_data(command)

    async def hold_flush(self, action: int):
        """
        Hold or flush the graphic engine.

        When held, new display commands are stored in memory and are displayed when the graphic engine is flushed.
        This allows stacking multiple graphic operations and displaying them simultaneously without screen flickering.
        The command is nested, the flush must be used the same number of times the hold was used
            action = 0 : Hold display
            action = 1 : Flush display
            action = 0xFF : Reset and flush all stacked hold. To be used when the state of the device is unknown
        """
        command = self.build_command(0x39, 0x00, action.to_bytes(1, byteorder='big'))
        await self.send_data(command)

    async def arc(self, x: int, y: int, r: int, angle_start: int, angle_end: int, thickness: int):
        """
        Draw an arc circle at the corresponding coordinates.
        Angles are in degrees, begin at 3 o'clock, and increase clockwise.
        """
        data = (x.to_bytes(2, byteorder='big') + y.to_bytes(2, byteorder='big') +
                r.to_bytes(1, byteorder='big') + angle_start.to_bytes(2, byteorder='big') +
                angle_end.to_bytes(2, byteorder='big') + thickness.to_bytes(1, byteorder='big'))
        command = self.build_command(0x3C, 0x00, data)
        await self.send_data(command)

# ------
# Layout

    async def layout_save(self, layout_parameters: GlassesLayoutParameters):
        """Save a layout"""
        command = self.build_command(0x60, 0x00, layout_parameters.build_parameters())
        await self.send_data(command)

    async def layout_delete(self, id: int):
        """
        Delete a layout
        if id = 0xFF, delete all layouts
        """
        command = self.build_command(0x61, 0x00, id.to_bytes(1, byteorder='big'))
        await self.send_data(command)

    async def layout_display(self, id: int, text: str):
        """Display text with layout id parameters"""
        data = id.to_bytes(1, byteorder='big') + text.encode('ascii')
        command = self.build_command(0x62, 0x00, data)
        await self.send_data(command)

    async def layout_clear(self, id: int):
        """Clears screen of the corresponding layout area"""
        data = id.to_bytes(1, byteorder='big')
        command = self.build_command(0x63, 0x00, data)
        await self.send_data(command)

    async def layout_list(self):
        """Give the list of saved layouts"""
        command = self.build_command(0x64, 0x00)
        await self.send_data(command)

    async def layout_position(self, id: int, x: int, y: int):
        """Redefine the position of a layout"""
        data = id.to_bytes(1, byteorder='big') + x.to_bytes(2, byteorder='big') + y.to_bytes(1, byteorder='big')
        command = self.build_command(0x65, 0x00, data)
        await self.send_data(command)

    async def layout_display_extended(self, id: int, x: int, y: int, text: str, extra_commands):
        """
        Display text with layout id at position x y
        The position is not saved
        extra_commands extra commands with the same format as used to save additional command to a layout
        """
        pass
        # data = id.to_bytes(1, byteorder='big') + x.to_bytes(2, byteorder='big') + y.to_bytes(1, byteorder='big')
        # command = self.build_command(0x65, 0x00, data)
        # await self.send_data(command)

    async def layout_get(self, id: int):
        """Get a layout parameters"""
        data = id.to_bytes(1, byteorder='big')
        command = self.build_command(0x67, 0x00, data)
        await self.send_data(command)

    async def layout_clear_extended(self, id: int, x: int, y: int):
        """Clears screen of the corresponding layout area"""
        data = id.to_bytes(1, byteorder='big') + x.to_bytes(2, byteorder='big') + y.to_bytes(1, byteorder='big')
        command = self.build_command(0x68, 0x00, data)
        await self.send_data(command)

    async def layout_clear_display(self, id: int, text: str):
        """Clear area and display text with layout id parameters"""
        data = id.to_bytes(1, byteorder='big') + text.encode('ascii')
        command = self.build_command(0x69, 0x00, data)
        await self.send_data(command)

    async def layout_clear_display_extended(self, id: int, x: int, y: int, text: str, extra_commands):
        """
        Clear area and display text with layout id at position x y
        The position is not saved
        extra_commands extra commands with the same format as used to save additional command to a layout
        """
        pass
        # data = id.to_bytes(1, byteorder='big') + x.to_bytes(2, byteorder='big') + y.to_bytes(1, byteorder='big')
        # command = self.build_command(0x65, 0x00, data)
        # await self.send_data(command)

# -------------
# Configuration

    async def cfg_write(self, name: str, version: int, password: int):
        """
            Write configuration, configurations are associated with layouts, images, etc
                - name: name of the configuration
                - version: provided by the user for tracking versions of configuration
                - password: if configuration already exists, the same password must be provided as the one used during the creation
        """
        data = (name.encode('ascii') + version.to_bytes(4, byteorder='big') +
                password.to_bytes(4, byteorder='big'))
        command = self.build_command(0xD0, 0x00, data)
        await self.send_data(command)

    async def cfg_read(self, name: str):
        """Get the number of elements stored in the configuration"""
        data = name.encode('ascii')
        command = self.build_command(0xD1, 0x00, data)
        await self.send_data(command)
    
    async def cfg_set(self, name: str):
        """Select the current configuration used to display layouts, images, etc"""
        data = name.encode('ascii')
        command = self.build_command(0xD2, 0x00, data)
        await self.send_data(command)

    async def cfg_list(self):
        """List configurations in memory"""
        command = self.build_command(0xD3, 0x00)
        await self.send_data(command)

    async def cfg_rename(self, old_name: str, new_name: str, password: int):
        """Rename a configuration"""
        data = old_name.encode("ascii") + new_name.encode('ascii') + password.to_bytes(4, byteorder='big')
        command = self.build_command(0xD4, 0x00, data)
        await self.send_data(command)

    async def cfg_delete(self, name: str):
        """Delete a configuration and all elements associated"""
        data = name.encode('ascii')
        command = self.build_command(0xD5, 0x00, data)
        await self.send_data(command)

    async def cfg_delete_less_used(self):
        """Delete the configuration that has not been used for the longest time"""
        command = self.build_command(0xD6, 0x00)
        await self.send_data(command)

    async def cfg_free_space(self):
        """Get free space available to store layouts, images, etc"""
        command = self.build_command(0xD7, 0x00)
        await self.send_data(command)

    async def cfg_get_nb(self):
        """Get number of configurations in memory"""
        command = self.build_command(0xD8, 0x00)
        await self.send_data(command)

# ------
# Device

    async def shutdown(self):
        """Shutdown the device"""
        command = self.build_command(0xE0, 0x00, bytearray([0x6F, 0x7F, 0xC4, 0xEE]))
        await self.send_data(command)

    async def reset(self):
        """Reset the device"""
        command = self.build_command(0xE1, 0x00, bytearray([0x5C, 0x1E, 0x2D, 0xE9]))
        await self.send_data(command)

    async def read_device_info(self, id: int):
        """
        Read a device information parameter
        id values:
            0: hw platform
            1: manufacturer
            2: advertising manufacturer id
            3: model
            4: sub-model
            5: fw version
            6: serial number
            7: battery model
            8: lens model
            9: display model
            10: display orientation
            11: certification 1
            12: certification 2
            13: certification 3
            14: certification 4
            15: certification 5
            16: certification 6
        """
        command = self.build_command(0xE3, 0x00, id.to_bytes(1, byteorder='big'))
        await self.send_data(command)

# #################################################################################################
# Communication ActiveLook -> Master

    async def tx_char_handler(self, char: BleakGATTCharacteristic, data: bytearray):
        unbuild = self.unbuild_command(data)
        if not unbuild:
            return
        
        command_id, command_format, length, command_data, query_id = unbuild
        match command_id:
            case 0x05:  # Battery
                args = int.from_bytes(command_data, "big"),

            case 0x06:  # Version
                fw_version = f"{command_data[0]}.{command_data[1]}.{command_data[2]}{chr(command_data[3])}"
                mfc_year = command_data[4]
                mfc_week = command_data[5]
                serial_number = f"{command_data[6]:02X}{command_data[7]:02X}{command_data[8]:02X}"

                args = fw_version, mfc_year, mfc_week, serial_number

            case 0x0A:  # Settings
                x = command_data[0]
                y = command_data[1]
                luma = command_data[2]
                als_enable = bool(command_data[3])
                gesture_enable = bool(command_data[4])

                args = x, y, luma, als_enable, gesture_enable

            case 0x64: # Layout list
                args = command_data

            case 0x67: # Layout get
                args = command_data

            case 0xD1: # Config Read
                version = int.from_bytes(command_data[0:4])
                nb_image = command_data[4]
                nb_layout = command_data[5]
                nb_font = command_data[6]
                nb_page = command_data[7]
                nb_gauge = command_data[8]

                args = version, nb_image, nb_layout, nb_font, nb_page, nb_gauge

            case 0xD3: # Config List
                args = []
                offset = 0

                while offset < len(command_data):
                    # Find the end of the NUL-terminated string
                    name_end = command_data.find(b'\x00', offset)
                    if name_end == -1:
                        name_end = 11
                    
                    # Extract the name
                    name = command_data[offset:name_end].decode('ascii')
                    offset = name_end + 1

                    # Extract the rest of the fields
                    size = int.from_bytes(command_data[offset:offset+4])
                    offset += 4

                    version = int.from_bytes(command_data[offset:offset+4])
                    offset += 4

                    usage_count = int.from_bytes(command_data[offset:offset+1])
                    offset += 1

                    install_count = int.from_bytes(command_data[offset:offset+1])
                    offset += 1

                    is_system = bool(int.from_bytes(command_data[offset:offset+1]))
                    offset += 1

                    entry = (name, size, version, usage_count, install_count, is_system)
                    args.append(entry)

            case 0xD7: # Config Free Space
                total_size = int.from_bytes(command_data[0:4])
                free_space = int.from_bytes(command_data[4:8])

                args = total_size, free_space

            case 0xD8: # Config Number
                args = int.from_bytes(command_data, "big"),
                
            case 0xE2: # Error
                cmdId = command_data[0].to_bytes(1).hex()
                error = command_data[1]
                subError = command_data[2]

                args = cmdId, error, subError

            case 0xE3: # Read Device Info
                args = command_data

            case _: # Default
                logging.warning(f"Unknown command ID received: {hex(command_id)}")
                return

        event = CommandEvent(command_id, args)
        await self.dispatcher.dispatch(event)

    def unbuild_command(self, command: bytearray) -> tuple[int, int, int, bytearray, bytearray] | None :
        start = 0xFF
        footer = 0xAA

        logging.debug(f"Frame received: {command.hex()}")

        if len(self.current_received_frame) == 0:
            if len(command) < 4:
                raise GlassesCommandException("Command length is less than expected minimum length.")

            if command[0] != start:
                raise GlassesCommandException("The first byte does not correspond to the start byte.")
            
            self.current_received_frame = command
            if command[-1] != footer:
                return
        else:
            if command[0] == start:
                raise GlassesCommandException("Error while receiving frame.")

            self.current_received_frame.extend(command)
            if command[-1] != footer:
                return

        command = self.current_received_frame
        logging.debug(f"Full command: {command.hex()}")

        command_id = command[1]
        command_format = command[2]

        logging.debug(f"Parsing command: command_id={hex(command_id)}, command_format={hex(command_format)}")

        if command_format & (1 << 4) == 0:
            length = command[3]
            data_start_index = 4
        else:
            length = int.from_bytes(command[3:5], byteorder='big')
            data_start_index = 5

        logging.debug(f"Length: {length}, Data start index: {data_start_index}")

        query_id_size = (command_format >> 1) & 0x0F

        logging.debug(f"Query ID size: {query_id_size}")

        if query_id_size > 0:
            query_id = command[data_start_index:data_start_index + query_id_size]
            data_start_index += query_id_size
            logging.debug(f"Query ID: {query_id.hex()}")
        else:
            query_id = b''
            logging.debug("No Query ID present")

        data = command[data_start_index:-1]

        logging.debug(f"Data: {data.hex()}")
        logging.debug("Command parsing successful.")

        self.current_received_frame = b''

        return command_id, command_format, length, data, query_id
