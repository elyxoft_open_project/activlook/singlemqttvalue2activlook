import asyncio
import logging
import logging.config
import signal
import sys
import time
import aiomqtt as mqtt
import argparse_custom as argparse
from typing import List

from glasses import Glasses, GlassesCommandException, GlassesLayoutParameters
from mqtt import MQTT
from cli import CommandManager
from event import EventDispatcher, CommandEvent

glasses: Glasses = None
mqtt_client: MQTT = None
command_manager: CommandManager =  CommandManager()
dispatcher: EventDispatcher = EventDispatcher()

# #################################################################################################
# Tasks

async def glasses_task():
    """Glasses task."""
    global glasses, dispatcher
    try:
        while not glasses:
            glasses = await Glasses.create(dispatcher)
            await asyncio.sleep(1)
        asyncio.create_task(user_input_task())
    except asyncio.CancelledError:
        if glasses and glasses.client:
            await glasses.client.disconnect()
        logging.info("Glasses task cancelled")

async def mqtt_task():
    """MQTT task"""
    global mqtt_client
    try:
        parser = argparse.ArgumentParser(description="")
        parser.add_argument("broker_uri", type=str, help="MQTT broker uri")
        parser.add_argument("username", type=str, help="MQTT broker username")
        parser.add_argument("password", type=str, help="MQTT broker password")
        parser.add_argument("topic", type=str, help="MQTT topic to subscribe to")

        try:
            parsed_args = parser.parse_args(sys.argv[1:])

            mqtt_client = MQTT(parsed_args.broker_uri, parsed_args.username, parsed_args.password)
            await mqtt_client.connect()
            # topic = "063076022110/PAPP"
            await mqtt_client.subscribe(parsed_args.topic, write_value_with_graph)
        except argparse.ArgumentError as e:
            logging.error(f"Argument parsing error: {str(e)}")
            #return  # Exit function early on error

    except asyncio.CancelledError:
        logging.info("MQTT task cancelled")

async def user_input_task():
    """User input task"""
    global command_manager, glasses
    prompt = "% "

    print("CLI started! Enter command:\n")
    while True:
        command = await asyncio.to_thread(input, prompt)
        command_parts = command.split()
        if not command_parts:
            #logging.error("Empty command.")
            continue
        cmd = command_parts[0]
        args = command_parts[1:]

        await command_manager.execute(cmd, glasses, args)

# #################################################################################################
# MQTT handler

SCREEN_WIDTH = 304
SCREEN_HEIGHT = 256
OFFSET_LEFT = 30
OFFSET_RIGHT = 30
OFFSET_TOP = 25
OFFSET_BOTTOM = 25

X_SCALE = 1  # Scale factor for the x-axis


graph_data = []

async def write_value_with_graph(message: mqtt.Message):
    global glasses, graph_data

    new_value = int(message.payload.decode('ascii'))

    await glasses.hold_flush(0)
    await glasses.color(0)
    await glasses.rectf(SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0)
    await glasses.color(15)
    await glasses.text(SCREEN_WIDTH - OFFSET_LEFT - 20, OFFSET_BOTTOM, 4, 0, 15, f"Value: {new_value}")

    current_time = time.time()

    if len(graph_data) == 0:
        # Initialize graph layout if it's the first value
        await glasses.cfg_write("mqtt", 1, 1234)
        layout_parameters = GlassesLayoutParameters(
            10,  # id
            OFFSET_RIGHT,  # x
            OFFSET_BOTTOM,  # y
            SCREEN_WIDTH - OFFSET_LEFT - OFFSET_RIGHT,  # width
            SCREEN_HEIGHT - OFFSET_TOP - OFFSET_BOTTOM,  # height
            15,  # fore_color
            0,  # back_color
            0,  # font
            False,  # text_valid
            0,  # text_x
            0,  # text_y
            0,  # text_rotation
            False  # text_opacity
        )
        layout_parameters.add_rect(1, 1, layout_parameters.width, layout_parameters.height)
        await glasses.layout_save(layout_parameters)
    await glasses.layout_display(10, " ")

    graph_data.append((new_value, current_time))

    # Remove old data points that exceed the screen width
    x_new = SCREEN_WIDTH - OFFSET_LEFT - ((current_time - graph_data[0][1]) * X_SCALE)
    while x_new <= OFFSET_RIGHT:
        graph_data.pop(0)
        x_new = SCREEN_WIDTH - OFFSET_LEFT - ((current_time - graph_data[0][1]) * X_SCALE)

    # Calculate min and max values for scaling
    min_value = min(graph_data, key=lambda x: x[0])[0]
    max_value = max(graph_data, key=lambda x: x[0])[0]

    if max_value == min_value:
        max_value += 1

    points = []
    for value, timestamp in graph_data:
        # Scale x and y coordinates
        x = SCREEN_WIDTH - OFFSET_LEFT - ((timestamp - graph_data[0][1]) * X_SCALE)
        y = (((value - min_value) / (max_value - min_value)) * (SCREEN_HEIGHT - OFFSET_TOP - OFFSET_BOTTOM - 5)) + OFFSET_BOTTOM + 5

        if len(points) < 2:
            points.append(int(x))
            points.append(int(y))
        else:
            last_x = points[-2]
            last_y = points[-1]
            if x != last_x:
                points.append(int(x))
                points.append(last_y)

            if y != last_y:
                points.append(int(x))
                points.append(int(y))

    # Draw the polyline
    if len(points) == 2:
        await glasses.point(points[0], points[1])
    else:
        i = 0
        chunk_size = 64
        while i < len(points):
            split_points = points[i:(i + chunk_size if i + chunk_size < len(points) else len(points))]
            await glasses.polyline(1, split_points)
            i += len(split_points)

    await glasses.hold_flush(1)        

# #################################################################################################
# Close program

async def close_program():
    """Cancel all running tasks."""
    tasks = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]
    logging.info(f"Cancelling {len(tasks)} tasks...")
    for task in tasks:
        task.cancel()
    await asyncio.gather(*tasks, return_exceptions=True)
    if glasses and glasses.client:
        await glasses.client.disconnect()
        logging.info("BLE client disconnected.")
    logging.info("Program closed.")
    asyncio.get_event_loop().stop()

def signal_handler(sig, frame):
    logging.info(f"Received signal {sig}. Closing program...")
    exit(0)

# #################################################################################################
# Glasses Commands
# -------
# General

@command_manager.register("display_power")
async def display_power_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Enable/disable power of the display", exit_on_error=False)
    parser.add_argument("enable", type=int, choices=[0, 1], help="Enable (1) or disable (0) display power")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.display_power(parsed_args.enable)

@command_manager.register("clear")
async def clear_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Clear the display memory (black screen)", exit_on_error=False)

    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.clear()

@command_manager.register("grey")
async def grey_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Set display grey level", exit_on_error=False)
    parser.add_argument("level", type=int, choices=range(0, 16), help="Grey level (0 to 15)")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.grey(parsed_args.level)

@command_manager.register("battery")
async def battery_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Get the battery level in %", exit_on_error=False)

    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.battery()

@command_manager.register("version")
async def version_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Get the device ID and firmware version", exit_on_error=False)

    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.version()

@command_manager.register("led")
async def led_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Set LED state", exit_on_error=False)
    parser.add_argument("state", type=int, choices=range(0, 4), help="LED state (0: Off, 1: On, 2: Toggle, 3: Blinking)")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.led(parsed_args.state)

@command_manager.register("shift")
async def shift_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Shift display objects", exit_on_error=False)
    parser.add_argument("-x", type=int, default=0, help="Shift in x direction (-128 to 127)")
    parser.add_argument("-y", type=int, default=0, help="Shift in y direction (-128 to 127)")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.shift(parsed_args.x, parsed_args.y)

@command_manager.register("settings")
async def settings_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Return the user parameters (shift, luma, sensor)", exit_on_error=False)

    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.settings()

# -----------------
# Display luminance

@command_manager.register("luma")
async def luma_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Set display luminance", exit_on_error=False)
    parser.add_argument("level", type=int, choices=range(0, 16), help="Luminance level (0 to 15)")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.luma(parsed_args.level)

# --------------
# Optical sensor

@command_manager.register("sensor")
async def sensor_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Turn on/off the auto-brightness adjustment and gesture detection", exit_on_error=False)
    parser.add_argument("enable", type=int, choices=[0, 1], help="Enable (1) or disable (0) auto-brightness adjustment and gesture detection")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.sensor(parsed_args.enable)

@command_manager.register("gesture")
async def gesture_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Turn on/off gesture detection", exit_on_error=False)
    parser.add_argument("enable", type=int, choices=[0, 1], help="Enable (1) or disable (0) gesture detection")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.gesture(parsed_args.enable)

@command_manager.register("auto_brightness")
async def auto_brightness_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Turn on/off the auto-brightness adjustment", exit_on_error=False)
    parser.add_argument("enable", type=int, choices=[0, 1], help="Enable (1) or disable (0) auto-brightness adjustment")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.auto_brightness(parsed_args.enable)

# --------
# Graphics

@command_manager.register("color")
async def color_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Sets the grey level (0 to 15) used to draw the next graphical element", exit_on_error=False)
    parser.add_argument("color", type=int, choices=range(0, 16), help="Grey level (0 to 15)")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.color(parsed_args.color)

@command_manager.register("point")
async def point_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Set a pixel on at the corresponding coordinates", exit_on_error=False)
    parser.add_argument("x", type=int, help="X coordinate")
    parser.add_argument("y", type=int, help="Y coordinate")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.point(parsed_args.x, parsed_args.y)

@command_manager.register("line")
async def line_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Draw a line at the corresponding coordinates", exit_on_error=False)
    parser.add_argument("x0", type=int, help="X0 coordinate")
    parser.add_argument("y0", type=int, help="Y0 coordinate")
    parser.add_argument("x1", type=int, help="X1 coordinate")
    parser.add_argument("y1", type=int, help="Y1 coordinate")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.line(parsed_args.x0, parsed_args.y0, parsed_args.x1, parsed_args.y1)

@command_manager.register("rect")
async def rect_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Draw an empty rectangle at the corresponding coordinates", exit_on_error=False)
    parser.add_argument("x0", type=int, help="X0 coordinate")
    parser.add_argument("y0", type=int, help="Y0 coordinate")
    parser.add_argument("x1", type=int, help="X1 coordinate")
    parser.add_argument("y1", type=int, help="Y1 coordinate")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.rect(parsed_args.x0, parsed_args.y0, parsed_args.x1, parsed_args.y1)

@command_manager.register("rectf")
async def rectf_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Draw a full rectangle at the corresponding coordinates", exit_on_error=False)
    parser.add_argument("x0", type=int, help="X0 coordinate")
    parser.add_argument("y0", type=int, help="Y0 coordinate")
    parser.add_argument("x1", type=int, help="X1 coordinate")
    parser.add_argument("y1", type=int, help="Y1 coordinate")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.rectf(parsed_args.x0, parsed_args.y0, parsed_args.x1, parsed_args.y1)

@command_manager.register("circ")
async def circ_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Draw an empty circle at the corresponding coordinates", exit_on_error=False)
    parser.add_argument("x", type=int, help="X coordinate")
    parser.add_argument("y", type=int, help="Y coordinate")
    parser.add_argument("r", type=int, help="Radius")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.circ(parsed_args.x, parsed_args.y, parsed_args.r)

@command_manager.register("circf")
async def circf_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Draw a full circle at the corresponding coordinates", exit_on_error=False)
    parser.add_argument("x", type=int, help="X coordinate")
    parser.add_argument("y", type=int, help="Y coordinate")
    parser.add_argument("r", type=int, help="Radius")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.circf(parsed_args.x, parsed_args.y, parsed_args.r)

@command_manager.register("text")
async def text_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Write text to display", exit_on_error=False)
    parser.add_argument("-x", type=int, default=274, help="X coordinate")
    parser.add_argument("-y", type=int, default=231, help="Y coordinate")
    parser.add_argument("-d", type=int, default=4, help="Direction")
    parser.add_argument("-f", type=int, default=2, help="Font")
    parser.add_argument("-c", type=int, default=15, help="Color")
    parser.add_argument("text", type=str, help="Text to display")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.text(parsed_args.x, parsed_args.y, parsed_args.d, parsed_args.f, parsed_args.c, parsed_args.text)

@command_manager.register("polyline")
async def polyline_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Draw multiple connected lines at the corresponding coordinates. Thickness is set for all lines.", exit_on_error=False)
    parser.add_argument("-t", type=int, default=1, help="Line thickness")
    parser.add_argument(
        'coordinates', 
        metavar='N', 
        type=int, 
        nargs='+', 
        help='Coordinates for drawing lines. Provide at least one set of coordinates, e.g., x0 y0 x1 y1 ...'
    )
    
    try:
        parsed_args = parser.parse_args(args)

        if len(parsed_args.coordinates) % 2 != 0:
            parser.error("Each coordinate must have an x and y value")
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    await glasses.polyline(parsed_args.t, parsed_args.coordinates)

@command_manager.register("hold_flush")
async def hold_flush_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(
        description = "Hold or flush the graphic engine.\
\
When held, new display commands are stored in memory and are displayed when the graphic engine is flushed.\
This allows stacking multiple graphic operations and displaying them simultaneously without screen flickering.\
The command is nested, the flush must be used the same number of times the hold was used\
    action = 0 : Hold display\
    action = 1 : Flush display\
    action = 0xFF : Reset and flush all stacked hold. To be used when the state of the device is unknown", 
        exit_on_error=False
    )
    parser.add_argument("action", type=int, choices=[0, 1, 0xFF], 
                        help="0 : Hold display\
1 : Flush display\
0xFF : Reset and flush all stacked hold. To be used when the state of the device is unknown"
    )
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.hold_flush(parsed_args.action)

@command_manager.register("arc")
async def arc_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(
        description="Draw an arc circle at the corresponding coordinates.\
Angles are in degrees, begin at 3 o'clock, and increase clockwise.", 
        exit_on_error=False
    )
    parser.add_argument("x", type=int, help="X coordinate")
    parser.add_argument("y", type=int, help="Y coordinate")
    parser.add_argument("r", type=int, help="Radius")
    parser.add_argument("angle_start", type=int, help="Angle start")
    parser.add_argument("angle_end", type=int, help="Angle end")
    parser.add_argument("thickness", type=int, help="Thickness")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.arc(parsed_args.x, parsed_args.y, parsed_args.r, parsed_args.angle_start, parsed_args.angle_end, parsed_args.thickness)

# ------
# Layout

@command_manager.register("layout_delete")
async def layout_delete_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Delete a layout", exit_on_error=False)
    parser.add_argument("id", type=int, help="id of the layout to be deleted. If id = 0xFF, delete all layouts")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.layout_delete(parsed_args.id)

@command_manager.register("layout_display")
async def layout_display_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Display text with layout id parameters", exit_on_error=False)
    parser.add_argument("id", type=int, help="id of the layout")
    parser.add_argument("text", type=str, help="Text to display")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.layout_display(parsed_args.id, parsed_args.text)

@command_manager.register("layout_clear")
async def layout_clear_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Clears screen of the corresponding layout area", exit_on_error=False)
    parser.add_argument("id", type=int, help="id of the layout")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.layout_clear(parsed_args.id)

@command_manager.register("layout_list")
async def layout_list_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Give the list of saved layouts", exit_on_error=False)
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.layout_list()

@command_manager.register("layout_position")
async def layout_position_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Redefine the position of a layout", exit_on_error=False)
    parser.add_argument("id", type=int, help="id of the layout")
    parser.add_argument("x", type=int, help="X coordinate")
    parser.add_argument("y", type=int, help="Y coordinate")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.layout_position(parsed_args.id, parsed_args.x, parsed_args.y)

@command_manager.register("layout_get")
async def layout_get_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Get a layout parameters", exit_on_error=False)
    parser.add_argument("id", type=int, help="id of the layout")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.layout_get(parsed_args.id)

@command_manager.register("layout_clear_extended")
async def layout_clear_extended_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Clears screen of the corresponding layout area", exit_on_error=False)
    parser.add_argument("id", type=int, help="id of the layout")
    parser.add_argument("x", type=int, help="X coordinate")
    parser.add_argument("y", type=int, help="Y coordinate")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.layout_clear_extended(parsed_args.id, parsed_args.x, parsed_args.y)

@command_manager.register("layout_clear_display")
async def layout_clear_display_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Clear area and display text with layout id parameters", exit_on_error=False)
    parser.add_argument("id", type=int, help="id of the layout")
    parser.add_argument("text", type=str, help="Text to display")
    
    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.layout_clear_display(parsed_args.id, parsed_args.text)

# -------------
# Configuration

@command_manager.register("cfg_write")
async def cfg_write_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Write configuration, configurations are associated with layouts, images, etc", exit_on_error=False)
    parser.add_argument("name", type=str, help="Name of the configuration")
    parser.add_argument("version", type=int, help="Version to track versions of configuration")
    parser.add_argument("password", type=int, help="If configuration already exists, the same password must be provided as the one used during the creation")
    
    try:
        parsed_args = parser.parse_args(args)

        if len(parsed_args.name) > 12:
            parser.error("Name maximum length is 12")
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.cfg_write(parsed_args.name, parsed_args.version, parsed_args.password)

@command_manager.register("cfg_read")
async def cfg_read_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Get the number of elements stored in the configuration", exit_on_error=False)
    parser.add_argument("name", type=str, help="Name of the configuration")

    try:
        parsed_args = parser.parse_args(args)

        if len(parsed_args.name) > 12:
            parser.error("Name maximum length is 12")
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.cfg_read(parsed_args.name)

@command_manager.register("cfg_set")
async def cfg_set_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Select the current configuration used to display layouts, images, etc", exit_on_error=False)
    parser.add_argument("name", type=str, help="Name of the configuration")

    try:
        parsed_args = parser.parse_args(args)

        if len(parsed_args.name) > 12:
            parser.error("Name maximum length is 12")
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.cfg_set(parsed_args.name)

@command_manager.register("cfg_list")
async def cfg_list_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="List configurations in memory", exit_on_error=False)

    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.cfg_list()

@command_manager.register("cfg_rename")
async def cfg_rename_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Rename a configuration", exit_on_error=False)
    parser.add_argument("old_name", type=str, help="Old name of the configuration")
    parser.add_argument("new_name", type=str, help="New name of the configuration")

    try:
        parsed_args = parser.parse_args(args)

        if len(parsed_args.old_name) > 12 or len(parsed_args.new_name) > 12:
            parser.error("Name maximum length is 12")
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.cfg_rename(parsed_args.old_name, parsed_args.new_name)

@command_manager.register("cfg_delete")
async def cfg_delete_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Delete a configuration and all elements associated", exit_on_error=False)
    parser.add_argument("name", type=str, help="Name of the configuration")

    try:
        parsed_args = parser.parse_args(args)

        if len(parsed_args.name) > 12:
            parser.error("Name maximum length is 12")
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.cfg_delete(parsed_args.name)

@command_manager.register("cfg_delete_less_used")
async def cfg_delete_less_used_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Delete the configuration that has not been used for the longest time", exit_on_error=False)

    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.cfg_delete_less_used()

@command_manager.register("cfg_free_space")
async def cfg_free_space_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Get free space available to store layouts, images, etc", exit_on_error=False)

    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.cfg_free_space()

@command_manager.register("cfg_get_nb")
async def cfg_get_nb_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Get number of configurations in memory", exit_on_error=False)

    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.cfg_get_nb()

# ------
# Device

@command_manager.register("shutdown")
async def shutdown_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Shutdown the device", exit_on_error=False)

    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.shutdown()

@command_manager.register("reset")
async def reset_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Reset the device", exit_on_error=False)

    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.reset()

@command_manager.register("read_device_info")
async def read_device_info_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Read a device information parameter", exit_on_error=False)
    parser.add_argument("id", type=int, choices=range(17), help="0: hw platform\n\
1: manufacturer\n\
2: advertising manufacturer id\n\
3: model\n\
4: sub-model\n\
5: fw version\n\
6: serial number\n\
7: battery model\n\
8: lens model\n\
9: display model\n\
10: display orientation\n\
11: certification 1\n\
12: certification 2\n\
13: certification 3\n\
14: certification 4\n\
15: certification 5\n\
16: certification 6")

    try:
        parsed_args = parser.parse_args(args)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error
    
    await glasses.read_device_info(parsed_args.id)

# ------------

@command_manager.register("send_raw_frame")
async def send_raw_frame_command(glasses: Glasses, args: List[str]):
    parser = argparse.ArgumentParser(description="Send raw frame.", exit_on_error=False)
    parser.add_argument("frame", type=str, help="Hexadecimal raw frame")

    try:
        parsed_args = parser.parse_args(args)
        try:
            data = bytearray.fromhex(parsed_args.frame if not parsed_args.frame.startswith('0x') else parsed_args.frame[2:])
            glasses.unbuild_command(data)
        except GlassesCommandException as e:
            parser.error(e)
        except ValueError as e:
            parser.error(e)
    except argparse.ArgumentError as e:
        logging.error(f"Argument parsing error: {str(e)}")
        return  # Exit function early on error


    
    await glasses.send_data(data)

# #################################################################################################
# CLI Handler

async def cli_handler(event: CommandEvent):
    data = event.data

    match event.command_id:
        case 0x05:  # Battery
            logging.info(f"Battery level: {data[0]}%")

        case 0x06:  # Version
            logging.info(f"version:\n\
- firmware version: {data[0]}\n\
- manufacturing year: {data[1]}\n\
- manufacturing week: {data[2]}\n\
- serial number: {data[3]}")

        case 0x0A:  # Settings
            logging.info(f"Settings:\n\
- global shift: ({data[0]}, {data[1]})\n\
- display luminance: {data[2]}\n\
- auto-brightness adjustment status: {'enable' if data[3] else 'disable'}\n\
- gesture detection status: {'enable' if data[4] else 'disable'}")
            
        case 0x64: # Layout list
            log = "Layout id list: \n  "
            for id in sorted(data):
                log += f"{id}, "
            logging.info(log)

        case 0x67: # Layout get
            logging.info(f"Layout parameters: {data.hex()}")
            
        case 0xD1: # Config Read
            logging.info(f"Number of elements stored in the configuration (version: {data[0]}):\n\
- image: {data[1]}\n\
- layout: {data[2]}\n\
- font: {data[3]}\n\
- page: {data[4]}\n\
- gauge: {data[5]}")

        case 0xD3: # Config List
            log = "Config list:\n"
            for cfg in data:
                log += f"  name: {cfg[0]}\n"
                log += f"    - size: {cfg[1]}\n"
                log += f"    - version: {cfg[2]}\n"
                log += f"    - usage count: {cfg[3]}\n"
                log += f"    - install count: {cfg[4]}\n"
                log += f"    - is system: {cfg[5]}\n"
            logging.info(log)

        case 0xD7: # Config Free Space
            logging.info(f"total size: {data[0]}, free space: {data[1]}")

        case 0xD8: # Config Number
            logging.info(f"Number of config: {data}")
            
        case 0xE2: # Error
            logging.error(f"Error received from ActiveLook:\n\
- command ID: 0x{data[0]}\n\
- error: {data[1]}\n\
- subError: {data[2]}")
            
        case 0xE3: # Read Device Info
            log = "Device information parameter:\n"
            try:
               log += f" - hex: {data.hex()}\n"
            except:
                pass
            try:
              log += f" - int: {int.from_bytes(data, signed=False)}\n"
            except:
                pass
            try:
                log += f" - uint: {int.from_bytes(data, signed=True)}\n"
            except:
                pass
            try:
                log += f" - str: {data.decode('ascii')}"
            except:
                pass
            logging.info(log)


# #################################################################################################
# Main

async def main():
    dispatcher.register(cli_handler)

    try:
        await asyncio.gather(glasses_task(), mqtt_task())
    except asyncio.CancelledError:
        logging.info("Main task cancelled")

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    for s in [signal.SIGINT, signal.SIGTERM]:
        loop.add_signal_handler(s, signal_handler, s, None)
    
    try:
        loop.run_until_complete(main())
    finally:
        loop.run_until_complete(close_program())
        loop.close()
        exit(0)
